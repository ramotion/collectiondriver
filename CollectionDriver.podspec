#
# Be sure to run `pod lib lint CollectionDriver.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CollectionDriver'
  s.version          = '0.1.1'
  s.summary          = 'Collection view helper library.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/ramotion/CollectionDriver'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Ramotion' => 'info@ramotion.com' }
  s.source           = { :git => 'https://github.com/ramotion/CollectionDriver.git', :tag => s.version.to_s }

  s.ios.deployment_target = '11.0'

  s.source_files = 'CollectionDriver/Classes/**/*'
  
  s.resource_bundles = {
    'CollectionDriver' => ['CollectionDriver/**/*.{lproj,storyboard,xcdatamodeld,xib,xcassets,json}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'utopia',  '~> 0.2.0'
  s.dependency 'SkeletonView', '~> 1.4'
  s.dependency 'TinyConstraints', '~> 4.0.0'
end
