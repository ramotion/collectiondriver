# CollectionDriver

[![CI Status](https://img.shields.io/travis/Alexander Telegin/CollectionDriver.svg?style=flat)](https://travis-ci.org/Alexander Telegin/CollectionDriver)
[![Version](https://img.shields.io/cocoapods/v/CollectionDriver.svg?style=flat)](https://cocoapods.org/pods/CollectionDriver)
[![License](https://img.shields.io/cocoapods/l/CollectionDriver.svg?style=flat)](https://cocoapods.org/pods/CollectionDriver)
[![Platform](https://img.shields.io/cocoapods/p/CollectionDriver.svg?style=flat)](https://cocoapods.org/pods/CollectionDriver)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CollectionDriver is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CollectionDriver'
```

## Author

Alexander Telegin, telegin.alexander@gmail.com

## License

CollectionDriver is available under the MIT license. See the LICENSE file for more info.
