//
//  Theme.swift
//  CollectionDriver
//
//  Created by Alex Telegin on 20/11/2018.
//

import UIKit

extension UIColor {
  internal static let unselected = #colorLiteral(red            : 0.8470588235, green            : 0.8470588235, blue            : 0.8470588235, alpha            : 1) // xD8D8D8 - unselected === skeleton
}

extension CGFloat {
  static let extraPadding      : CGFloat = 30
  static let elementSpacing    : CGFloat = 24
}
