import Foundation

enum BuildConfiguration {
  case debug
  case release
}

#if DEBUG
let buildConfiguration = BuildConfiguration.debug
#else
let buildConfiguration = BuildConfiguration.release
#endif

struct ErrorMessage {

  let title: String
  let description: String
  let justText: String

  init(_ error: Error) {
    switch buildConfiguration {
    case .debug:
      switch error {
      case let error as DecodingError:
        title = "Decoding Error"
        description = String(describing: error)
        justText = error.localizedDescription

      default:
        title = String(describing: type(of: error))
        description = String(describing: error)
        justText = error.localizedDescription
      }

    case .release:
      title = String.General.error
      description = error.localizedDescription
      justText = error.localizedDescription
    }
  }
}
