import UIKit
import TinyConstraints

extension UIView {

  @discardableResult
  func edgesToSuperview(top: CGFloat = 0, left: CGFloat = 0,
                        bottom: CGFloat = 0, right: CGFloat = 0) -> Constraints {
    self.translatesAutoresizingMaskIntoConstraints = false
    let top = self.topToSuperview(offset: top)
    let left = self.leftToSuperview(offset: left)
    let bottom = self.bottomToSuperview(offset: -bottom)
    let right = self.rightToSuperview(offset: right)
    return [top, left, bottom, right]
  }

  @discardableResult
  func edgesToSuperview(padding: CGFloat = 0) -> Constraints {
    return self.edgesToSuperview(top: padding, left: padding, bottom: padding, right: padding)
  }
}
