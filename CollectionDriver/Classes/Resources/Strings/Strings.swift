
import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
internal extension String {

  enum General {
        /// Error
        internal static let error = String.tr("Localizable", "general.error")
    }

  enum EmptyState {
    /// No Alerts
    internal static let noAlerts = String.tr("Localizable", "empty_state.no_alerts")
    /// No Conversations
    internal static let noConversations = String.tr("Localizable", "empty_state.no_conversations")
    /// No Data
    internal static let noData = String.tr("Localizable", "empty_state.no_data")
    /// No Products
    internal static let noProducts = String.tr("Localizable", "empty_state.no_products")
    /// No Reviews
    internal static let noReviews = String.tr("Localizable", "empty_state.no_reviews")
    /// Nothing to see here
    internal static let nothingToSee = String.tr("Localizable", "empty_state.nothing_to_see")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension String {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
