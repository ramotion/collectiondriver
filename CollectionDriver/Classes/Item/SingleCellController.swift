import UIKit
import utopia
//import VendefyService

protocol Fillable {
    associatedtype Data: Hashable
    func fill(_ data: Data)
}

final class SingleCellController<CollectionCell: UICollectionViewCell & Fillable, ItemData>: CollectionItemProtocol where CollectionCell.Data == ItemData {
  
  typealias Cell = CollectionCell
  typealias Data = ItemData
  
  var cellSelected: ((Cell, Data) -> ())?
  var cellConfigure: ((Cell) -> ())?
  
  var data: ItemData
  var events = CollectionItemEvents<Data>()
  
  let height: CGFloat
  
  init(data: ItemData, height: CGFloat) {
    self.data = data
    self.height = height
  }

  // MARK: CollectionItemProtocol
  func fillCell(_ cell: CollectionCell, animated: Bool) {
    cell.fill(data)
    cellConfigure?(cell)
  }
  
  func size(constrainedTo width: CGFloat, height: CGFloat) -> CGSize {
    return CGSize(width: width, height: self.height)
  }
  
  func cellSelected(_ cell: Cell) {
    cellSelected?(cell, data)
  }
}


final class ViewCellController<T:UIView>: CollectionItemProtocol {
  
  typealias Cell = UICollectionViewCell
  typealias Data = None
  
  var view: T
  var data: None = none
  var events = CollectionItemEvents<Data>()
  
  let height: CGFloat
  
  init(view: T, height: CGFloat) {
    self.view = view
    self.height = height
  }
  
  // MARK: CollectionItemProtocol
  func fillCell(_ cell: Cell, animated: Bool) {
    view.add(to: cell.contentView).edgesToSuperview()
  }
  
  func size(constrainedTo width: CGFloat, height: CGFloat) -> CGSize {
    return CGSize(width: width, height: self.height)
  }
  
  func cellSelected(_ cell: Cell) {}
}
