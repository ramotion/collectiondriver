import Foundation
import UIKit
import TinyConstraints

public struct EmptyView {

  public enum Theme {
    public static let defaultCenterOffset: CGFloat = -80
  }
  
  public static func create(image: UIImage) -> UIView {
    return EmptyView.default(image: image, title: String.EmptyState.nothingToSee)
  }
  
  public static let clear: UIView = UIView()
  
  public static func `default`(image: UIImage, title: String, offset: CGFloat = Theme.defaultCenterOffset) -> UIView {
    let result = UIView()
    
    let container = UIStackView.stackContainer(distribution: .fill).add(to: result)
    container.edgesToSuperview()

    container.addSpace(height: CGFloat.elementSpacing)

    UIImageView(image: image).add(toStack: container, alignment: .center).do {
      $0.widthToSuperview(multiplier: 0.8)
      $0.aspectRatio(1)
      $0.contentMode = .scaleAspectFit
    }
    
    UILabel().add(toStack: container, padding: CGFloat.extraPadding).do {
      $0.numberOfLines = 0
      $0.font = UIFont.systemFont(ofSize: 20)
      $0.textColor = UIColor.unselected
      $0.textAlignment = .center
      $0.text = title
    }
    
    return result
  }
  
  public static func info(title: String, offset: CGFloat = Theme.defaultCenterOffset) -> UIView {
    let result = UIView()
    
    UILabel().add(to: result).do {
      $0.center(in: result, offset: CGPoint(x: 0, y: offset))
      $0.numberOfLines = 0
      $0.font = UIFont.systemFont(ofSize: 20)
      $0.textColor = UIColor.unselected
      $0.textAlignment = .center
      $0.text = title
    }
    
    return result
  }
}


