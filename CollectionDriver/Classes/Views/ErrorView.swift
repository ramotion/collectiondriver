import UIKit

final class ErrorView: UIView {

  var error: Error? {
    didSet {
      guard let error = error else { return }
      showError(error)
    }
  }

  private let label = UILabel()


  // MARK: - Init

  override init(frame: CGRect) {
    super.init(frame: frame)

    label.do {
      $0.add(to: self)
      $0.centerYToSuperview(offset: -20)
      $0.leadingToSuperview(offset: 20)
      $0.trailingToSuperview(offset: 20)
      $0.numberOfLines = 0
      $0.textAlignment = .center
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }


  // MARK: -

  private func showError(_ error: Error) {
    let msg = ErrorMessage(error)
    label.text = msg.description
  }
}
